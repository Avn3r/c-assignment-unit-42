﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ApplicationLogic;
using ObjectGettersSetters;
using DataAccess;
using Microsoft.VisualBasic;

namespace CSharp_Assignment
{
    public partial class StudentForm : Form
    {
        public StudentForm()
        {
            InitializeComponent();
        }

        private void IntiStudentList()
        {
            //var students is loaded with Student list
            var students = StudentService.Load();

            foreach (var student in students)
            {
                this.txtName.Text = student.Name;
                this.txtSurname.Text = student.Surname;
                this.txtSubject.Text = student.Subject;
                this.txtIdCard.Text = student.IdCard;
                this.txtClass.Text = student.ClassStudent;
                this.txtAddress.Text = student.Address;

                this.txtDateOfBirth.Text = Convert.ToString(student.DOB);
                this.txtContactNo.Text = Convert.ToString(student.ContactNo);
            }
        }

        private void StudentForm_Load(object sender, EventArgs e)
        {
            //initialize the method IntiStudentList
            IntiStudentList();

            //the button Updated will be disabled on Form Load Event
            this.btnUpdate.Enabled = false;
        }

        public void clearComponents()
        {
            this.txtAddress.Text = "";
            this.txtName.ResetText();
            this.txtSurname.ResetText();
            this.txtSubject.Text = "";
            this.txtClass.Text = "";
            this.txtContactNo.Text = "";
            this.txtDateOfBirth.Text = "";
            this.txtIdCard.Text = "";
            this.txtSubject.Text = "";
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            //Calling the clearComponent method on Clear Button Click Event
            clearComponents();
        }

        //===========================================================================================================//
        //======================================     ON LEAVE EVENT - START     =====================================//
        //===========================================================================================================//

        private void txtName_Leave(object sender, EventArgs e)
        {
            //Checking that the entered characters are only Alphabetical Characters (lowercase or UPPERCASE)
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtName.Text, "^[a-zA-Z ]"))
            {
                MessageBox.Show("This textbox accepts only Alphabetical Characters." + this.txtName.Text + " is not valid.", "Text Input", MessageBoxButtons.OK, MessageBoxIcon.Information);

                //Component text is reset
                this.txtName.ResetText();

                //Component text is given back focus
                this.txtName.Focus();
            }

        }

        private void txtSurname_Leave(object sender, EventArgs e)
        {
            //Checking that the entered characters are only Alphabetical Characters (lowercase or UPPERCASE)
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtSurname.Text, "^[a-zA-Z ]"))
            {
                MessageBox.Show("This textbox accepts only Alphabetical Characters." + this.txtSurname.Text + " is not valid.", "Text Input", MessageBoxButtons.OK, MessageBoxIcon.Information);

                //Component text is reset
                this.txtSurname.ResetText();

                //Component text is given back focus
                this.txtSurname.Focus();
            }
        }

        private void txtAddress_Leave(object sender, EventArgs e)
        {
            //Checking that the entered characters are either Alphabetical or Numerical Characters, or both combined
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtAddress.Text, "^[a-zA-Z0-9 ]"))
            {
                MessageBox.Show("This textbox accepts both Alphabetical and Numeric Characters.", "Text Input", MessageBoxButtons.OK, MessageBoxIcon.Information);

                //Component text is reset
                this.txtAddress.ResetText();

                //Component text is given back focus
                this.txtAddress.Focus();
            }
        }

        private void txtIdCard_Leave(object sender, EventArgs e)
        {
            //Checking that the entered characters are either Alphabetical or Numerical Characters, or both combined
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtIdCard.Text, "^[a-zA-Z0-9]"))
            {
                MessageBox.Show("This textbox accepts both Alphabetical and Numeric Characters.", "Text Input", MessageBoxButtons.OK, MessageBoxIcon.Information);


                //Component text is reset
                this.txtIdCard.ResetText();

                //Component text is given back focus
                this.txtIdCard.Focus();
            }
        }

        private void txtContactNo_Leave(object sender, EventArgs e)
        {
            //Checking that the entered characters are only Numerical Characters
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtContactNo.Text, "^[0-9]"))
            {
                MessageBox.Show("This textbox accepts only Numeric Characters.", "Text Input", MessageBoxButtons.OK, MessageBoxIcon.Information);

                //Component text is reset
                this.txtContactNo.ResetText();

                //Component text is given back focus
                this.txtContactNo.Focus();
            }
        }

        private void txtClass_Leave(object sender, EventArgs e)
        {
            //Checking that the submitted characters are either Alphabetical or Numerical Characters, or both combined
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtClass.Text, "^[a-zA-Z0-9]"))
            {
                MessageBox.Show("This textbox accepts both Alphabetical and Numeric Characters.", "Text Input", MessageBoxButtons.OK, MessageBoxIcon.Information);

                //Component text is reset
                this.txtClass.ResetText();

                //Component text is given back focus
                this.txtClass.Focus();
            }
        }

        private void txtSubject_Leave(object sender, EventArgs e)
        {
            //Checking that the submitted characters are either Alphabetical or Numerical Characters, or both combined
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtSubject.Text, "^[a-zA-Z0-9]"))
            {
                MessageBox.Show("This textbox accepts both Alphabetical and Numeric Characters.", "Text Input", MessageBoxButtons.OK, MessageBoxIcon.Information);

                //Component text is reset
                this.txtSubject.ResetText();

                //Component text is given back focus
                this.txtSubject.Focus();
            }
        }

        public void SearchStudent()
        {
            //Calling the clear component method to clear text from the latter
            clearComponents();

            String userInput = Microsoft.VisualBasic.Interaction.InputBox("Please enter Student ID Card", "Search Student Record");

            var students = StudentService.Load(userInput);

            //Checking if user input matches a record in our Student folder
            //and is not null.
            if (students != null)
            {
                this.txtName.Text = students.Name;
                this.txtSurname.Text = students.Surname;
                this.txtSubject.Text = students.Subject;
                this.txtIdCard.Text = students.IdCard;
                this.txtClass.Text = students.ClassStudent;
                this.txtAddress.Text = students.Address;

                this.txtDateOfBirth.Text = Convert.ToString(students.DOB);
                this.txtContactNo.Text = Convert.ToString(students.ContactNo);

                //the Update button will be rendered available
                this.btnUpdate.Enabled = true;
            }

            else
            {
                //the Update button will be rendered unavailable again if the search of a record (using ID Card) fails
                MessageBox.Show("No records found with given Student IDCard: " + userInput, "Search output");
                
                this.btnUpdate.Enabled = false;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            //calling the searchStudent method
            SearchStudent();
        }

        public void UpdateStudent()
        {
            Student myStudent = new Student();
            myStudent.Name = this.txtName.Text;
            myStudent.Surname = this.txtSurname.Text;
            myStudent.Address = this.txtAddress.Text;
            myStudent.IdCard = this.txtIdCard.Text;
            myStudent.Subject = this.txtSubject.Text;
            myStudent.ClassStudent = this.txtClass.Text;

            myStudent.DOB = DateTime.Parse(this.txtDateOfBirth.Text.ToString());
            myStudent.ContactNo = Convert.ToInt32(this.txtContactNo.Text);


            //updating the Student record
            StudentService.Save(myStudent);

            //displaying a message to the user informing that the Student Record Update was successfully implemented
            MessageBox.Show("Student Record Updated Successfully", "Update Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);

            //setting the button update to disabled after the record update is done
            this.btnUpdate.Enabled = false;

            //calling the clear component method to dispose from any text
            clearComponents();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //calling the UpdateStudent method
            UpdateStudent();
        }

        //==========================================================================================================//
        //==================================     ADD STUDENT (BUTTON) - START     ==================================//
        //==========================================================================================================//

        public void AddStudent()
        {
            //creating a new EMTPY instance of the Student object
            Student myStudent = new Student();

            //creating attributes for our Student object
            String studentID, name, surname, address, subject, StudentClass;
            int contact = 0;
            DateTime dob;
            Boolean quit = false;

            do
            {
                //Assigning the user input in textbox txtName to the string variable name
                name = this.txtName.Text;

                if (name != null)
                {
                    if (name == "")
                    {
                        //Error message to the user if Name Textbox is empty
                        MessageBox.Show("Student's name is required. Please do not leave the name empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                        //Setting the Error provider to the name component with the specified error, upon identifying that there is an empty Name TextBox (once Add button is clicked)
                        this.erpProvider.SetError(this.txtName, "Student's name is required.");

                        quit = true;

                        break;
                    }

                    else
                    {
                        quit = false;

                        this.erpProvider.Dispose();
                    }

                }

                else
                {
                    return;
                }

                //Assigning the user input in textbox txtSurname to the string variable surname
                surname = this.txtSurname.Text;

                if (surname != null)
                {
                    if (surname == "")
                    {
                        //Error message to the user if Surname Textbox is empty
                        MessageBox.Show("Student's surname is required. Please do not leave the surname empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                        //Setting the Error provider to the Surname component with the specified error, upon identifying that there is an empty Surname TextBox (once Add button is clicked)
                        this.erpProvider.SetError(this.txtSurname, "Student's surname is required.");

                        quit = true;

                        break;
                    }

                    else
                    {
                        quit = false;

                        this.erpProvider.Dispose();
                    }

                }

                else
                {
                    return;
                }

                //Assigning the user input in textbox txtAddress to the string variable address
                address = this.txtAddress.Text;

                if (address != null)
                {
                    if (address == "")
                    {
                        //Error message to the user if Address Textbox is empty
                        MessageBox.Show("Student's address is required. Please do not leave the address empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                        //Setting the Error provider to the Address component with the specified error, upon identifying that there is an empty Address TextBox (once Add button is clicked)
                        this.erpProvider.SetError(this.txtAddress, "Student's address is required.");

                        quit = true;

                        break;
                    }

                    else
                    {
                        quit = false;

                        this.erpProvider.Dispose();
                    }

                }

                else
                {
                    return;
                }

                //Assigning the user input in textbox txtSubject to the string variable subject
                subject = this.txtSubject.Text;

                if (subject != null)
                {
                    if (subject == "")
                    {
                        //Error message to the user if Subject Textbox is empty
                        MessageBox.Show("Student's subject is required. Please do not leave the subject empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                        //Setting the Error provider to the Subject component with the specified error, upon identifying that there is an empty Subject TextBox (once Add button is clicked)
                        this.erpProvider.SetError(this.txtSubject, "Student's subject is required.");

                        quit = true;

                        break;
                    }

                    else
                    {
                        quit = false;

                        this.erpProvider.Dispose();
                    }

                }

                else
                {
                    return;
                }

                //Assigning the user input in textbox txtClass to the string variable class
                StudentClass = this.txtClass.Text;

                if (StudentClass != null)
                {
                    if (StudentClass == "")
                    {
                        //Error message to the user if Class Textbox is empty
                        MessageBox.Show("Student's class is required. Please do not leave the class empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                        //Setting the Error provider to the Class component with the specified error, upon identifying that there is an empty Class TextBox (once Add button is clicked)
                        this.erpProvider.SetError(this.txtClass, "Student's class is required.");

                        quit = true;

                        break;
                    }

                    else
                    {
                        quit = false;

                        this.erpProvider.Dispose();
                    }

                }

                else
                {
                    return;
                }

                //Assigning the user input in textbox txtIdCard to the string variable studentID
                studentID = this.txtIdCard.Text;

                if (studentID != null)
                {
                    if (studentID == "")
                    {
                        //Error message to the user if IdCard Textbox is empty
                        MessageBox.Show("Student's ID is required. Please do not leave the ID empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                        //Setting the Error provider to the IdCard component with the specified error, upon identifying that there is an empty IdCard TextBox (once Add button is clicked)
                        this.erpProvider.SetError(this.txtIdCard, "Student's ID is required.");

                        quit = true;

                        break;
                    }

                    else
                    {
                        quit = false;

                        this.erpProvider.Dispose();
                    }

                }

                else
                {
                    return;
                }

                //Assigning the user input in textbox txtContactNo to the string variable contact
                contact = int.Parse(this.txtContactNo.Text);

                if (contact != 0)
                {
                    if (contact == 0)
                    {
                        //Error message to the user if ContactNo Textbox is empty
                        MessageBox.Show("Student's contact is required. Please do not leave the contact empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                        //Setting the Error provider to the ContactNo component with the specified error, upon identifying that there is an empty ContactNo TextBox (once Add button is clicked)
                        this.erpProvider.SetError(this.txtContactNo, "Student's contact is required.");

                        quit = true;

                        break;
                    }

                    else
                    {
                        quit = false;

                        this.erpProvider.Dispose();
                    }

                }

                else
                {
                    return;
                }

                //Assigning the user input in textbox txtDateOfBirth to the string variable dob
                dob = DateTime.Parse(this.txtDateOfBirth.Text);

                if (dob != null)
                {
                    if (dob > DateTime.Today)
                    {
                        //Error message to the user if DateOfBirth Masked Textbox is empty
                        MessageBox.Show("Student's Date Of Birth is invalid. Please submit a valid Date Of Birth.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                        //Setting the Error provider to the DateOfBirth component with the specified error, upon identifying that there is an empty DateOfBirth Masked TextBox (once Add button is clicked)
                        this.erpProvider.SetError(this.txtDateOfBirth, "Student's Date Of Birth is required.");

                        quit = true;

                        break;
                    }

                    else
                    {
                        quit = false;

                        this.erpProvider.Dispose();
                    }

                }

                else
                {
                    return;
                }

                myStudent.Name = name;
                myStudent.Surname = surname;
                myStudent.Address = address;
                myStudent.IdCard = studentID;
                myStudent.Subject = subject;
                myStudent.DOB = dob;
                myStudent.ContactNo = contact;
                myStudent.ClassStudent = StudentClass;

                StudentService.Save(myStudent);

                MessageBox.Show("New Student successfully added to the Database.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);

                clearComponents();

            } while (quit == true);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddStudent();
        }

        private void StudentForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Once Student Form is closed, user is re-directed back to the Welcome Form
            Welcome myWelcomeScreen = new Welcome();
            myWelcomeScreen.Show();
        }
    }
}