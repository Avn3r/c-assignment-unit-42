﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CSharp_Assignment
{
    public partial class Welcome : Form
    {
        public Welcome()
        {
            InitializeComponent();
        }

        private void btnTeacher_Click(object sender, EventArgs e)
        {
            //Upon click, Load new Teacher Form
            TeacherForm myTeacherForm = new TeacherForm();
            myTeacherForm.Show();
            this.Hide();
        }

        private void btnStudent_Click(object sender, EventArgs e)
        {
            //Upon click, Load new Student Form
            StudentForm myStudentForm = new StudentForm();
            myStudentForm.Show();
            this.Hide();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            //Exit Application
            Application.Exit();
        }

        private void Welcome_Load(object sender, EventArgs e)
        {

        }
    }
}
