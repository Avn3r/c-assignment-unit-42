﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ApplicationLogic;
using ObjectGettersSetters;
using DataAccess;
using Microsoft.VisualBasic;

namespace CSharp_Assignment
{
    public partial class TeacherForm : Form
    {
        public TeacherForm()
        {
            InitializeComponent();
        }

        private void IntiTeacherList()
        {
            //var teachers is loaded with Teacher list
            var teachers = TeacherService.Load();

            foreach (var teacher in teachers)
            {
                this.txtName.Text = teacher.Name;
                this.txtSurname.Text = teacher.Surname;
                this.txtSubject.Text = teacher.Subject;
                this.txtIdCard.Text = teacher.IdCard;
                this.txtClass.Text = teacher.ClassTeacher;
                this.txtAddress.Text = teacher.Address;

                this.txtDateOfBirth.Text = Convert.ToString(teacher.DOB);
                this.txtContactNo.Text = Convert.ToString(teacher.ContactNo);
                this.txtSalary.Text = Convert.ToString(teacher.Salary);
            }
        }

        private void TeacherForm_Load(object sender, EventArgs e)
        {
            //initialize the method IntiTeacherList
            IntiTeacherList();

            //the button Updated will be disabled on Form Load Event
            this.btnUpdate.Enabled = false;
        }

        public void clearComponents()
        {
            this.txtAddress.Text = "";
            this.txtName.ResetText();
            this.txtSurname.ResetText();
            this.txtSubject.Text = "";
            this.txtClass.Text = "";
            this.txtContactNo.Text = "";
            this.txtDateOfBirth.Text = "";
            this.txtIdCard.Text = "";
            this.txtSalary.Text = "";
            this.txtSubject.Text = "";
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            //Calling the clearComponent method on Clear Button Click Event
            clearComponents();
        }

        //===========================================================================================================//
        //======================================     ON LEAVE EVENT - START     =====================================//
        //===========================================================================================================//

        private void txtName_Leave(object sender, EventArgs e)
        {
            //Checking that the entered characters are only Alphabetical Characters (lowercase or UPPERCASE)
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtName.Text, "^[a-zA-Z ]"))
            {
                MessageBox.Show("This textbox accepts only Alphabetical Characters." + this.txtName.Text + " is not valid.", "Text Input", MessageBoxButtons.OK, MessageBoxIcon.Information);

                //Component text is reset
                this.txtName.ResetText();

                //Component text is given back focus
                this.txtName.Focus();
            }

        }

        private void txtSurname_Leave(object sender, EventArgs e)
        {
            //Checking that the entered characters are only Alphabetical Characters (lowercase or UPPERCASE)
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtSurname.Text, "^[a-zA-Z ]"))
            {
                MessageBox.Show("This textbox accepts only Alphabetical Characters." + this.txtSurname.Text + " is not valid.", "Text Input", MessageBoxButtons.OK, MessageBoxIcon.Information);

                //Component text is reset
                this.txtSurname.ResetText();

                //Component text is given back focus
                this.txtSurname.Focus();
            }
        }

        private void txtAddress_Leave(object sender, EventArgs e)
        {
            //Checking that the entered characters are either Alphabetical or Numerical Characters, or both combined
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtAddress.Text, "^[a-zA-Z0-9 ]"))
            {
                MessageBox.Show("This textbox accepts both Alphabetical and Numeric Characters.", "Text Input", MessageBoxButtons.OK, MessageBoxIcon.Information);

                //Component text is reset
                this.txtAddress.ResetText();

                //Component text is given back focus
                this.txtAddress.Focus();
            }
        }

        private void txtIdCard_Leave(object sender, EventArgs e)
        {
            //Checking that the entered characters are either Alphabetical or Numerical Characters, or both combined
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtIdCard.Text, "^[a-zA-Z0-9]"))
            {
                MessageBox.Show("This textbox accepts both Alphabetical and Numeric Characters.", "Text Input", MessageBoxButtons.OK, MessageBoxIcon.Information);

                //Component text is reset
                this.txtIdCard.ResetText();

                //Component text is given back focus
                this.txtIdCard.Focus();
            }
        }

        private void txtContactNo_Leave(object sender, EventArgs e)
        {
            //Checking that the entered characters are only Numerical Characters
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtContactNo.Text, "^[0-9]"))
            {
                MessageBox.Show("This textbox accepts only Numeric Characters.", "Text Input", MessageBoxButtons.OK, MessageBoxIcon.Information);

                //Component text is reset
                this.txtContactNo.ResetText();

                //Component text is given back focus
                this.txtContactNo.Focus();
            }
        }

        private void txtSalary_Leave(object sender, EventArgs e)
        {
            //Checking that the entered characters are only Numerical Characters
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtSalary.Text, "^[0-9.]"))
            {
                MessageBox.Show("This textbox accepts only Numeric Characters.", "Text Input", MessageBoxButtons.OK, MessageBoxIcon.Information);

                //Component text is reset
                this.txtSalary.ResetText();

                //Component text is given back focus
                this.txtSalary.Focus();
            }
        }

        private void txtClass_Leave(object sender, EventArgs e)
        {
            //Checking that the submitted characters are either Alphabetical or Numerical Characters, or both combined
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtClass.Text, "^[a-zA-Z0-9]"))
            {
                MessageBox.Show("This textbox accepts both Alphabetical and Numeric Characters.", "Text Input", MessageBoxButtons.OK, MessageBoxIcon.Information);

                //Component text is reset
                this.txtClass.ResetText();

                //Component text is given back focus
                this.txtClass.Focus();
            }
        }

        private void txtSubject_Leave(object sender, EventArgs e)
        {
            //Checking that the submitted characters are either Alphabetical or Numerical Characters, or both combined
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtSubject.Text, "^[a-zA-Z0-9]"))
            {
                MessageBox.Show("This textbox accepts both Alphabetical and Numeric Characters.", "Text Input", MessageBoxButtons.OK, MessageBoxIcon.Information);

                //Component text is reset
                this.txtSubject.ResetText();

                //Component text is given back focus
                this.txtSubject.Focus();
            }
        }

        public void SearchTeacher()
        {
            //Calling the clear component method to clear text from the latter
            clearComponents();

            String userInput = Microsoft.VisualBasic.Interaction.InputBox("Please enter Teacher ID Card.", "Search Teacher Record");

            var teachers = TeacherService.Load(userInput);

            //Checking if user input matches a record in our Teacher folder
            //and is not null.
            if (teachers != null)
            {
                this.txtName.Text = teachers.Name;
                this.txtSurname.Text = teachers.Surname;
                this.txtSubject.Text = teachers.Subject;
                this.txtIdCard.Text = teachers.IdCard;
                this.txtClass.Text = teachers.ClassTeacher;
                this.txtAddress.Text = teachers.Address;

                this.txtDateOfBirth.Text = Convert.ToString(teachers.DOB);
                this.txtContactNo.Text = Convert.ToString(teachers.ContactNo);
                this.txtSalary.Text = Convert.ToString(teachers.Salary);

                //the Update button will be rendered available
                this.btnUpdate.Enabled = true;
            }

            else
            {
                //the Update button will be rendered unavailable again if the search of a record (using ID Card) fails
                MessageBox.Show("No records found with given Teacher IDCard: " + userInput, "Search output");

                this.btnUpdate.Enabled = false;
            }
        }
        
        private void btnSearch_Click(object sender, EventArgs e)
        {
            //Calling the searchTeacher method
            SearchTeacher();
        }

        public void UpdateTeacher()
        {
            Teacher myTeacher = new Teacher();
            myTeacher.Name = this.txtName.Text;
            myTeacher.Surname = this.txtSurname.Text;
            myTeacher.Address = this.txtAddress.Text;
            myTeacher.IdCard = this.txtIdCard.Text;
            myTeacher.Subject = this.txtSubject.Text;
            myTeacher.ClassTeacher = this.txtClass.Text;

            myTeacher.DOB = DateTime.Parse(this.txtDateOfBirth.Text.ToString());
            myTeacher.Salary = float.Parse(this.txtSalary.Text);
            myTeacher.ContactNo = Convert.ToInt32(this.txtContactNo.Text);


            //Updating the Teacher record
            TeacherService.Save(myTeacher);

            //displaying a message to the user informing that the Teacher Record Update was successfully implemented
            MessageBox.Show("Teacher Record Updated Successfully.", "Update Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);

            //setting the button update to disabled after the record update is done
            this.btnUpdate.Enabled = false;

            //calling the clear component method to dispose from any text
            clearComponents();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //calling the UpdateTeacher method
            UpdateTeacher();
        }

        //==========================================================================================================//
        //==================================     ADD STUDENT (BUTTON) - START     ==================================//
        //==========================================================================================================//

        public void AddTeacher()
        {
            //creating a new EMTPY instance of the Teacher object
            Teacher myTeacher = new Teacher();

            //creating attributes for our Teacher object
            String teacherID, name, surname, address, subject, TeacherClass;
            int contact = 0;
            float salary = 0f;
            DateTime dob;
            Boolean quit = false;

            do
            {
                //Assigning the user input in textbox txtName to the string variable name
                name = this.txtName.Text;

                if (name != null)
                {
                    if (name == "")
                    {
                        //Error message to the user if Name Textbox is empty
                        MessageBox.Show("Teacher's name is required. Please do not leave the name empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                        //Setting the Error provider to the name component with the specified error, upon identifying that there is an empty Name TextBox (once Add button is clicked)
                        this.erpProvider.SetError(this.txtName, "Teacher's name is required.");

                        quit = true;

                        break;
                    }

                    else
                    {
                        quit = false;

                        this.erpProvider.Dispose();
                    }

                }

                else
                {
                    return;
                }

                //Assigning the user input in textbox txtSurname to the string variable surname
                surname = this.txtSurname.Text;

                if (surname != null)
                {
                    if (surname == "")
                    {
                        //Error message to the user if Surname Textbox is empty
                        MessageBox.Show("Teacher's surname is required. Please do not leave the surname empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                        //Setting the Error provider to the Surname component with the specified error, upon identifying that there is an empty Surname TextBox (once Add button is clicked)
                        this.erpProvider.SetError(this.txtSurname, "Teacher's surname is required.");

                        quit = true;

                        break;
                    }

                    else
                    {
                        quit = false;

                        this.erpProvider.Dispose();
                    }

                }

                else
                {
                    return;
                }

                //Assigning the user input in textbox txtAddress to the string variable address
                address = this.txtAddress.Text;

                if (address != null)
                {
                    if (address == "")
                    {
                        //Error message to the user if Address Textbox is empty
                        MessageBox.Show("Teacher's address is required. Please do not leave the address empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                        //Setting the Error provider to the Address component with the specified error, upon identifying that there is an empty Address TextBox (once Add button is clicked)
                        this.erpProvider.SetError(this.txtAddress, "Teacher's address is required.");

                        quit = true;

                        break;
                    }

                    else
                    {
                        quit = false;

                        this.erpProvider.Dispose();
                    }

                }

                else
                {
                    return;
                }

                //Assigning the user input in textbox txtSubject to the string variable subject
                subject = this.txtSubject.Text;

                if (subject != null)
                {
                    if (subject == "")
                    {
                        //Error message to the user if Subject Textbox is empty
                        MessageBox.Show("Teacher's subject is required. Please do not leave the subject empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                        //Setting the Error provider to the Subject component with the specified error, upon identifying that there is an empty Subject TextBox (once Add button is clicked)
                        this.erpProvider.SetError(this.txtSubject, "Teacher's subject is required.");

                        quit = true;

                        break;
                    }

                    else
                    {
                        quit = false;

                        this.erpProvider.Dispose();
                    }

                }

                else
                {
                    return;
                }

                //Assigning the user input in textbox txtClass to the string variable class
                TeacherClass = this.txtClass.Text;

                if (TeacherClass != null)
                {
                    if (TeacherClass == "")
                    {
                        //Error message to the user if Class Textbox is empty
                        MessageBox.Show("Teacher's class is required. Please do not leave the class empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                        //Setting the Error provider to the Class component with the specified error, upon identifying that there is an empty Class TextBox (once Add button is clicked)
                        this.erpProvider.SetError(this.txtClass, "Teacher's class is required.");

                        quit = true;

                        break;
                    }

                    else
                    {
                        quit = false;

                        this.erpProvider.Dispose();
                    }

                }

                else
                {
                    return;
                }

                //Assigning the user input in textbox txtIdCard to the string variable teacherID
                teacherID = this.txtIdCard.Text;

                if (teacherID != null)
                {
                    if (teacherID == "")
                    {
                        //Error message to the user if IdCard Textbox is empty
                        MessageBox.Show("Teacher's ID is required. Please do not leave the ID empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                        //Setting the Error provider to the IdCard component with the specified error, upon identifying that there is an empty IdCard TextBox (once Add button is clicked)
                        this.erpProvider.SetError(this.txtIdCard, "Teacher's ID is required.");

                        quit = true;

                        break;
                    }

                    else
                    {
                        quit = false;

                        this.erpProvider.Dispose();
                    }

                }

                else
                {
                    return;
                }

                //Assigning the user input in textbox txtContactNo to the string variable contact
                contact = int.Parse(this.txtContactNo.Text);

                if (contact != 0)
                {
                    if (contact == 0)
                    {
                        //Error message to the user if ContactNo Textbox is empty
                        MessageBox.Show("Teacher's contact is required. Please do not leave the contact empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                        //Setting the Error provider to the ContactNo component with the specified error, upon identifying that there is an empty ContactNo TextBox (once Add button is clicked)
                        this.erpProvider.SetError(this.txtContactNo, "Teacher's contact is required.");

                        quit = true;

                        break;
                    }

                    else
                    {
                        quit = false;

                        this.erpProvider.Dispose();
                    }

                }

                else
                {
                    return;
                }

                //Assigning the user input in textbox txtDateOfBirth to the string variable dob
                dob = DateTime.Parse(this.txtDateOfBirth.Text);

                if (dob != null)
                {
                    if (dob > DateTime.Today)
                    {
                        //Error message to the user if DateOfBirth Masked Textbox is empty
                        MessageBox.Show("Teacher's Date Of Birth is invalid. Please submit a valid Date Of Birth.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                        //Setting the Error provider to the DateOfBirth component with the specified error, upon identifying that there is an empty DateOfBirth Masked TextBox (once Add button is clicked)
                        this.erpProvider.SetError(this.txtDateOfBirth, "Teacher's Date Of Birth is required.");

                        quit = true;

                        break;
                    }

                    else
                    {
                        quit = false;

                        this.erpProvider.Dispose();
                    }

                }

                else
                {
                    return;
                }

                //Assigning the user input in textbox txtSalary to the string variable salary
                salary = float.Parse(this.txtSalary.Text);

                if (salary != 0)
                {
                    if (salary == 0)
                    {
                        //Error message to the user
                        MessageBox.Show("Teacher's salary is required. Please do not leave the salary empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                        //Setting the Error provider to the salary component with the specified error
                        this.erpProvider.SetError(this.txtSalary, "Teacher's salary is required.");

                        quit = true;

                        break;
                    }

                    else
                    {
                        quit = false;

                        this.erpProvider.Dispose();
                    }

                }

                else
                {
                    return;
                }

                myTeacher.Name = name;
                myTeacher.Surname = surname;
                myTeacher.Address = address;
                myTeacher.IdCard = teacherID;
                myTeacher.Subject = subject;
                myTeacher.Salary = salary;
                myTeacher.DOB = dob;
                myTeacher.ContactNo = contact;
                myTeacher.ClassTeacher = TeacherClass;

                TeacherService.Save(myTeacher);

                MessageBox.Show("New Teacher successfully added to the Database.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);

                clearComponents();

            } while (quit == true);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddTeacher();
        }

        private void TeacherForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Once Teacher Form is closed, user is re-directed back to the Welcome Form
            Welcome myWelcomeScreen = new Welcome();
            myWelcomeScreen.Show();
        }
    }
}
