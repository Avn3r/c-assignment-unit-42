﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using ObjectGettersSetters;

namespace DataAccess
{
    public class StudentDA
    {
        //Declaring constant variable to create Student Folder path in the solution root folder
        private const string StudentFolder = "data\\students\\";

        //Declaring constant variable to set the StudentFile extention to .bin
        private const string StudentsFileTypeExntension = ".bin";

        //Default Constructor
        public StudentDA() { }

        private static void CreateStudentFolder()

        {
            //Checking into the Operating System directory if the Student folder exists or not
            if (!Directory.Exists(StudentFolder))
            {
                //If the folder does not exist the application will add the Student folder into the directory
                Directory.CreateDirectory(StudentFolder);
            }

        }

        private static string GetStudentFilePath(string idCardNo)
        {
            //Calling the CreateStudentFolder method to check if the Folder exists to save the student file in
            CreateStudentFolder();

            //setting the file path with file name and extension
            string targetPath = string.Format(StudentFolder + "{0}" + StudentsFileTypeExntension, idCardNo);

            return targetPath;
        }

        public static bool Save(Student student)
        {
            try
            {
                //Making an instance of the binary formatter to save the object of type Student
                BinaryFormatter myBinaryFormatter = new BinaryFormatter();

                //Setting the target path from the Student's ID
                string targetPath = GetStudentFilePath(student.IdCard);

                //Making use of the USING (ERROR HANDLING) functionality to dispose of data in case of unhandled error exception
                using (StreamWriter myStreamWriter = new StreamWriter(targetPath, false, Encoding.UTF8))
                {
                    //Serialization
                    myBinaryFormatter.Serialize(myStreamWriter.BaseStream, student);
                }
                return true;
            }
            catch (System.Runtime.Serialization.SerializationException se)
            {
                //Showing the exception
                throw se;
            }
        }

        public static Student Load(string idCardNo)
        {
            //Setting the target Path with the idCardNo
            string targetPath = GetStudentFilePath(idCardNo);

            //Checking if file exists
            if (File.Exists(targetPath))
            {
                //Getting the target path formatted through the idcard
                BinaryFormatter myBinaryFormatter = new BinaryFormatter();

                //StreamReader to read the serialized student object
                using (StreamReader myStreamReader = new StreamReader(targetPath))
                {
                    //deserialization
                    return myBinaryFormatter.Deserialize(myStreamReader.BaseStream) as Student;
                }
            }

            return null;

        }

        //Load method that returns Students object to a list
        public static List<Student> Load()
        {
            //Creating an instance of the list with data type Student using Generics
            List<Student> myList = new List<Student>();

            //Passing the location of the students folder
            string targetPath = StudentFolder;

            //Checking if the folder already exists in the directory
            if (Directory.Exists(targetPath))
            {
                //If targetpath exists
                DirectoryInfo myDirectoryInfo = new DirectoryInfo(targetPath);

                //Loading all files with .bin extension into the file info array
                FileInfo[] files = myDirectoryInfo.GetFiles("*" + StudentsFileTypeExntension);

                //Making an instance of BinaryFormatter
                BinaryFormatter myBinaryFormatter = new BinaryFormatter();

                //For Loop to iterate all the files within the File Array
                foreach (var file in files)
                {
                    string path = file.FullName;
                    using (StreamReader myStreamReader = new StreamReader(path))
                    {
                        //Adding student object to my list and do deserialization
                        myList.Add(myBinaryFormatter.Deserialize(myStreamReader.BaseStream) as Student);
                    }
                }
            }

            //Return the list of objects of type Student
            return myList;

        }
    }
}