﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using ObjectGettersSetters;

namespace DataAccess
{
    public class TeacherDA
    {
        //Declaring constant variable to create Teacher Folder path in the solution root folder
        private const string TeacherFolder = "data\\teachers\\";

        //Declaring constant variable to set the TeacherFile extention to .bin
        private const string TeachersFileTypeExntension = ".bin";

        //Default Constructor
        public TeacherDA() { }

        private static void CreateTeacherFolder()

        {
            //Checking into the Operating System directory if the Teacher folder exists or not
            if (!Directory.Exists(TeacherFolder))
            {
                //If the folder  does not exist the application will add the Teacher folder into the directory
                Directory.CreateDirectory(TeacherFolder);
            }

        }

        private static string GetTeacherFilePath(string idCardNo)
        {
            //Calling the CreateTeacherFolder method to check if the Folder exists to save the teacher file in
            CreateTeacherFolder();

            //setting the file path with file name and extension
            string targetPath = string.Format(TeacherFolder + "{0}" + TeachersFileTypeExntension, idCardNo);

            return targetPath;
        }

        public static bool Save(Teacher teacher)
        {
            try
            {
                //Making an instance of the binary formatter to save the object of type Teacher
                BinaryFormatter myBinaryFormatter = new BinaryFormatter();

                //Setting the target path from the Teacher's ID
                string targetPath = GetTeacherFilePath(teacher.IdCard);

                //Making use of the USING (ERROR HANDLING) functionality to enforce the disposal of data in case of unhandled error exception
                using (StreamWriter myStreamWriter = new StreamWriter(targetPath, false, Encoding.UTF8))
                {
                    //Serialization
                    myBinaryFormatter.Serialize(myStreamWriter.BaseStream, teacher);
                }
                return true;
            }
            catch (System.Runtime.Serialization.SerializationException se)
            {
                //Showing the exception
                throw se;
            }
        }

        public static Teacher Load(string idCardNo)
        {
            //Setting the target Path with the idCardNo
            string targetPath = GetTeacherFilePath(idCardNo);

            //Checking if file exists
            if (File.Exists(targetPath))
            {
                //Getting the target path formatted through the idcard
                BinaryFormatter myBinaryFormatter = new BinaryFormatter();

                //StreamReader to read the serialized teacher object
                using (StreamReader myStreamReader = new StreamReader(targetPath))
                {
                    //deserialization
                    return myBinaryFormatter.Deserialize(myStreamReader.BaseStream) as Teacher;
                }
            }

            return null;

        }

        //Load method that returns Teachers object to a list
        public static List<Teacher> Load()
        {
            //Creating an instance of the list with data type Teacher using Generics
            List<Teacher> myList = new List<Teacher>();

            //Passing the location of the teachers folder
            string targetPath = TeacherFolder;

            //Checking if the folder already exists in the directory
            if (Directory.Exists(targetPath))
            {
                //If targetpath exists
                DirectoryInfo myDirectoryInfo = new DirectoryInfo(targetPath);

                //Loading all files with .bin extension into the file info array
                FileInfo[] files = myDirectoryInfo.GetFiles("*" + TeachersFileTypeExntension);

                //Making an instance of BinaryFormatter
                BinaryFormatter myBinaryFormatter = new BinaryFormatter();

                //For Loop to iterate all the files within the File Array
                foreach (var file in files)
                {
                    string path = file.FullName;
                    using (StreamReader myStreamReader = new StreamReader(path))
                    {
                        //Adding teacher object to my list and do deserialization
                        myList.Add(myBinaryFormatter.Deserialize(myStreamReader.BaseStream) as Teacher);
                    }
                }
            }

            //Return the list of objects of type Teacher
            return myList;

        }
    }
}